import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useTotalStore = defineStore('total', () => {
    const total = ref<number>(0);

    function increment(amount: number): void {
        total.value = total.value + amount;
    }

    function reset(): void {
        total.value = 0;
    }

    return { total, increment, reset };
}, {
    persist: true,
});
